import { Component, OnInit } from '@angular/core';

import { Products } from '../model/products';
import { Fermentation } from '../model/fermentation';

import { YeastService } from '../services/yeast.service';

import { CookieService } from 'ngx-cookie-service';

/**
 *class related with products.component.html
 *
 * @export
 * @class ProductsComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  //properties
  objProducts: Products;
  arraypProducts: Fermentation[] = [];
  cookieObj: any;

  constructor(private yeastService: YeastService,
    private cookieService: CookieService, ) { }

  /**
   *method invoked when loading the page
   *
   * @memberof ProductsComponent
   */
  ngOnInit() {
    this.arraypProducts = this.yeastService.createFermentationSelect2();
    this.inicializeForm();
    this.getCookie();
  }

  /**
   *method that inicialize values of form 
   *
   * @memberof ProductsComponent
   */
  inicializeForm() {
    if (!this.objProducts) {
      this.objProducts = new Products();
      this.objProducts.fermentation = new Fermentation();
      this.objProducts.fermentation = this.arraypProducts[1];
    }
  }

  /**
   *method invoked when I click botton Submit. Create a cookie.
   *
   * @memberof ProductsComponent
   */
  getCookie() {
    if (this.cookieService.check("objProducts")) {
      this.cookieObj = JSON.parse(this.cookieService.get("objProducts"));

      Object.assign(this.objProducts, this.cookieObj);

      this.objProducts.fermentation = 
      this.arraypProducts[this.cookieObj._fermentation._id];
    }
  }

  /**
   *method invoked when I click botton Submit. Call cookie and show product object 
   *
   * @memberof ProductsComponent
   */
  yeastEntryProducts(): void {
    this.cookieService.set("objProducts", JSON.stringify(this.objProducts));
    console.log(this.objProducts);
  }

  /**
   *methos invoked when I ask for the image address
   *
   * @param {*} event
   * @memberof ProductsComponent
   */
  onFileChange(event){
    if (event) this.objProducts.image = event.target.files[0].name;
    console.log(event);
  }
}
