import { Component, OnInit } from '@angular/core';
import { Yeast } from '../model/yeast';
import { YeastService } from '../services/yeast.service';

/**
 *componente relacionado con yeast-management.component.html
 *
 * @export
 * @class YeastManagementComponent
 * @implements {OnInit}
 */

@Component({
  selector: 'app-yeast-management',
  templateUrl: './yeast-management.component.html',
  styleUrls: ['./yeast-management.component.css']
})

export class YeastManagementComponent implements OnInit {

  tempFilter: number = 500;
  idFilter: string = "";

  currentPage: number;
  itemsPerPage: number;

  yeasts: Yeast[] = []; //  filtered state
  yeastsFiltered: Yeast[] = [];//estado actual 

  yeastSelected: Yeast;

  constructor(private yeastService: YeastService) { }

  /**
   *
   *method invoked when loading the page
   * @memberof YeastManagementComponent
   */
  ngOnInit() {
    this.yeasts = this.yeastService.generateYeastsRandom();
    this.yeastsFiltered = this.yeasts;

    this.tempFilter = 500;
    this.itemsPerPage = 10;
    this.currentPage = 1;
  }

  /**
   *method that filters a yeast when I whan to filter by temperature  or 
   *filter by Id Specie
   * @memberof YeastManagementComponent
   */
  filter() {
    this.yeastsFiltered = this.yeasts.filter(yeast => {
      let tempValid: boolean = false;
      let idValid: boolean = false;

      tempValid = (yeast.temperature <= this.tempFilter);
      if (this.idFilter && this.idFilter != "") {
        if (yeast.id.toLowerCase().indexOf(this.idFilter.toLowerCase()) != -1) {
          idValid = true;
        }
      } else {
        idValid = true;
      }
      return idValid && tempValid;

    })
  }

  /**
   *
   *Remove yeast when I click botton X
   * @param {Yeast} rv
   * @memberof YeastManagementComponent
   */
  removeRes(rv: Yeast) {
    let founds = this.yeastsFiltered.length;

    if (founds > this.yeastsFiltered.length) { // delete one 
      this.yeasts.splice(this.yeasts.indexOf(rv), 1);
    }
    this.yeastsFiltered.splice(this.yeastsFiltered.indexOf(rv), 1);

  }

  /**
   *method showing yeast
   *
   * @param {Yeast} rv
   * @memberof YeastManagementComponent
   */
  goTo(rv: Yeast) {
    this.yeastSelected=rv;
  }

}
