import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YeastManagementComponent } from './yeast-management.component';

describe('YeastManagementComponent', () => {
  let component: YeastManagementComponent;
  let fixture: ComponentFixture<YeastManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YeastManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YeastManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
