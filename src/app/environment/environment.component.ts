import { Component, OnInit } from '@angular/core';
import { Environment } from '../model/environment';
import { CookieService } from 'ngx-cookie-service';

/**
 *class related with environment.component.html
 *
 * @export
 * @class EnvironmentComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-environment',
  templateUrl: './environment.component.html',
  styleUrls: ['./environment.component.css']
})

export class EnvironmentComponent implements OnInit {
  //properties
  objEnvironment: Environment;
  cookieObj: any;
  constructor(private cookieService: CookieService) { }

  /**
   *method invoked when loading the page
   *
   * @memberof EnvironmentComponent
   */
  ngOnInit() {
    this.objEnvironment = new Environment();
    this.getCookie();
  }

  /**
   *method invoked when I click botton Submit. Call cookie and show environment object 
   *
   * @memberof EnvironmentComponent
   */
  getEnvironments() {
    this.cookieService.set("objEnvironment", JSON.stringify(this.objEnvironment));
    console.log(this.objEnvironment)
  }

  /**
   *method invoked when I click botton Submit. Create a cookie.
   *
   * @memberof EnvironmentComponent
   */
  getCookie() {
    if (this.cookieService.check("objEnvironment")) {
      this.cookieObj = JSON.parse(this.cookieService.get("objEnvironment"));

      Object.assign(this.objEnvironment, this.cookieObj);

      this.objEnvironment = new Environment(this.cookieObj._geography, this.cookieObj._weather);
    }
  }

}
